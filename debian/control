Source: libmodule-want-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: libfile-path-tiny-perl,
                     libtest-carp-perl,
                     perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libmodule-want-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libmodule-want-perl.git
Homepage: https://metacpan.org/release/Module-Want

Package: libmodule-want-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: module to check @INC only once for wanted modules
 Sometimes you want to lazy load a module for use in, say, a loop or function.
 First you do the eval-require but then realize if the module is not available
 it will re-search @INC each time. So then you add a lexical boolean to your
 eval and do the same simple logic all over the place.
 .
 Module::Want encapsulates that logic so that have_mod() is like eval {
 require X; 1 } but if the module can't be loaded it will remember that fact
 and not look in @INC again on subsequent calls.
